import { makeExecutableSchema } from 'graphql-tools';
import { queries } from './queries';
import { mutations } from './mutations';
import { typeDefs } from '../models/typeDefs';
import { MongoDB } from '../database/Mongo';

const advResolvers = function(db :MongoDB) {
  return {
    Query: queries(db),
    Mutation: mutations(db)
  };
};

export const schema = function(db :MongoDB) {
  const resolvers = advResolvers(db);
  return makeExecutableSchema({
    typeDefs,
    resolvers
  });
};
