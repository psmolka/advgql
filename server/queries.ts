import { MongoDB } from '../database/Mongo';
import { Token } from '../models/Token';

export const queries = function(db :MongoDB) {
  return {
    users: function(_ :any, token: Token) {
      return new Promise((resolve, reject) => {
        if (!token) {
          return reject("Not authorized");
        }
        db.user.findOne({'token.hash': token.hash}, (err, user) => {
          if(user) {
            db.user.find({'token.hash': token.hash}, (err, users) => {
              return resolve(users);
            });
          } else {
            return reject("Not authorized");
          }
        });
      });
    }
  }
};
