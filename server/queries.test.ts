import { first } from 'lodash';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import { IUser } from '../models/User';
import { MongoDB } from '../database/Mongo';
import { server, getQuery } from '../server/server';
import { TestDB, TestUser, TestServer } from '../test/Constants';

@suite
class QueriesTests {
  before() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
    this.db.user.count({}, (err, res) => {
      expect(res).to.be.equal(0);
    }).exec();
    this.httpServer = server(TestServer.port, TestDB);
  }

  after() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
    this.httpServer.close();
  }

  private db: MongoDB;
  private httpServer;

  constructor() {
    this.db = TestDB;
  }

  @test("should throw connection error")
  public badServerRequest() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery({ hostname: "fakeServerHostname", port: 9999 }, 'query users {user { username }}')
          .catch(err => {
            expect(err).not.to.be.undefined;
          });
      });
  }

  @test("should respond with error of bad query")
  public wrongQuery() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery(TestServer, 'query users {user { username }}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res: any) => {
        const errors = res.errors;
        expect(errors).length.to.be.above(0);
      });
  }

  @test("should get users list with gql from server")
  public getUsers() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery(TestServer, 'mutation {getToken(username: "testUsername", password: "testPassword") {hash}}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res :any) => {
        return getQuery(TestServer, 'query {users(hash:' + res.data.getToken.hash + ') { username }}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res: any) => {
        const users = res.data.users;
        expect(users.length).to.be.equal(1);
        expect(users[0].username).to.be.equal(TestUser.username);
      });
  }
}
