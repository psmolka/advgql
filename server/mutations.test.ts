import { first } from 'lodash';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import { IUser } from '../models/User';
import { MongoDB } from '../database/Mongo';
import { server, getQuery } from '../server/server';
import { TestDB, TestUser, TestServer } from '../test/Constants';

@suite
class MutationsTests {
  before() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
    this.db.user.count({}, (err, res) => {
      expect(res).to.be.equal(0);
    }).exec();
    this.httpServer = server(TestServer.port, TestDB);
  }

  after() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
    this.httpServer.close();
  }

  private db: MongoDB;
  private httpServer;

  constructor() {
    this.db = TestDB;
  }

  @test("should send correct credentials and get token")
  public getToken() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery(TestServer, 'mutation {getToken(username: "testUsername", password: "testPassword") {_id, hash, createdAt}}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res: any) => {
        expect(res.data.getToken._id).not.to.be.undefined;
        expect(res.data.getToken._id).not.to.be.null;
        expect(res.data.getToken._id).length.to.be.above(0);
        expect(res.data.getToken.hash).not.to.be.undefined;
        expect(res.data.getToken.hash).not.to.be.null;
        expect(res.data.getToken.hash).length.to.be.above(0);
      });
  }

  @test("should send bad credentials and not get token")
  public wrongPassword() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery(TestServer, 'mutation {getToken(username: "testUsername", password: "wrongPassword") {hash, createdAt}}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res: any) => {
        expect(res.data.getToken).to.be.null;
      });
  }

  @test("should send bad username and not get user not found error")
  public wrongUsername() {
    return new this.db.user(TestUser).save()
      .then(() => {
        return getQuery(TestServer, 'mutation {getToken(username: "wrongUsername", password: "wrongPassword") {hash, createdAt}}')
          .then(queryResult => {
            return queryResult;
          });
      })
      .then((res :any) => {
        expect(res.errors[0].message).to.be.equal('User not found');
        expect(res.data.getToken).to.be.null;
      });
  }
}
