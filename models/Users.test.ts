/// <reference path="../node_modules/mocha-typescript/globals.d.ts" />

import { first } from 'lodash';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import * as http from 'http'
import * as bcrypt from 'bcrypt';
import { IUser } from '../models/User';
import { MongoDB } from '../database/Mongo';
import { server } from '../server/server';
import { TestDB, TestUser } from '../test/Constants';

let chai = require('chai');

@suite
class UserPasswordTests {
  before() {
    this.db.user.remove({}).exec();
  }
  after() {
    this.db.user.remove({}).exec();
  }
  private db: MongoDB;

  constructor() {
    this.db = TestDB;
  }

  @test('should create new user')
  public createUserWithHashedPassword() {
    return new this.db.user(TestUser).save()
      .then((result: IUser) => {
        expect(result).not.to.be.undefined;
        expect(result.username).to.be.equal(TestUser.username);
        expect(result.password).not.to.be.undefined;
        expect(result.password).not.to.be.equal(TestUser.password);
        expect(result.email).to.be.equal(TestUser.email);

        return new Promise((resolve, reject) => {
          bcrypt.compare(TestUser.password, result.password, (err, res) => {
            expect(err).to.be.undefined;
            expect(res).to.be.true;
            resolve(res);
          })
        })
      });
  }
}
