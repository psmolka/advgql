import { Token } from '../models/Token';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';

@suite
class TokenTests {

  @test("should create valid token")
  public createToken() {
    const testToken = new Token();
    expect(testToken).not.to.be.undefined;
    expect(testToken).not.to.be.null;
    expect(testToken instanceof Token).to.be.true;
    expect(testToken.hash).not.to.be.undefined;
    expect(testToken.hash).to.be.above(0);
    expect(testToken.hash).not.to.be.string;
    expect(testToken.createdAt).not.to.be.undefined;
    expect(testToken.createdAt).to.be.string;
  }
}
