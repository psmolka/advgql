import { Schema, Types } from 'mongoose';
import * as bcrypt from 'bcrypt';

export class Token {
  _id: Types.ObjectId;
  hash :number;
  createdAt :Date;

  constructor() {
    this.hash = Math.round(Date.now() / Math.random());
    this.createdAt = new Date();
  }
}

export const TokenSchema: Schema = new Schema({
  hash: Number,
  createdAt: { type: Date, default: Date.now }
});
