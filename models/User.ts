import { Document, Schema, Model, model} from "mongoose";
import * as bcrypt from 'bcrypt';
import { last } from 'lodash';
import { Token, TokenSchema } from './Token';

const SALT = 10;


export interface IUser {
  username: string,
  password: string,
  email: string,
  firstName: string,
  lastName: string,
  token: Token
}

export interface IUserModel extends IUser, Document {}

export const userSchema: Schema = new Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  email: String,
  firstName: String,
  lastName: String,
  token: TokenSchema
});

userSchema.pre('save', function(next) {
  let user = this;
  //if (!user.isModified('password')) {
    //return next();
  //}
  bcrypt.genSalt(SALT, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
        user.password = hash;
        next();
    });
  });
});
