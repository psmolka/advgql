/// <reference path="../node_modules/mocha-typescript/globals.d.ts" />

import { first } from 'lodash';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import * as http from 'http'
import { IUser } from '../models/User';
import { MongoDB } from '../database/Mongo';
import { server } from '../server/server';
import { TestDB, TestUser } from '../test/Constants';

let chai = require('chai');

@suite
class MongoDBTests {
  before() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
    this.db.user.count({}, (err, res) => {
      expect(res).to.be.equal(0);
    }).exec();
  }
  after() {
    this.db.user.remove({}, err => {
      expect(err).to.be.null;
    }).exec();
  }
  private db: MongoDB;

  constructor() {
    this.db = TestDB;
  }

  @test('should create new user document in MongoDB')
  public createUserInDB() {

    return new this.db.user(TestUser).save()
      .then((result: IUser) => {

        expect(result).not.to.be.undefined;
        expect(result.username).to.be.equal(TestUser.username);
        expect(result.password).not.to.be.undefined;
        expect(result.email).to.be.equal(TestUser.email);
      });
  }

}
