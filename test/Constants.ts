import { MongoDB } from '../database/Mongo';

export const MongoURL: string = 'mongodb://localhost:27017/tests';
export const TestDB = new MongoDB(MongoURL);
export const TestServer = { hostname: 'localhost', port: 8888 };

export const TestUser = {
  username: 'testUsername',
  password: 'testPassword',
  email: 'testEmail'
};
