import { server } from './server/server';
import { MongoDB } from './database/Mongo';

export const db = new MongoDB("mongodb://localhost:27017/advdev");

server(8080, db);
